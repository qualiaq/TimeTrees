using System;
using System.IO;
using System.Text;
using System.Globalization;
using Newtonsoft.Json;

namespace TimeTrees
{
    struct TimelineEvent
    {
        public DateTime Date;
        public string Passage;
        
    }
    struct Person
    {
        public int Id;
        public string Name;
        public DateTime Birthday;
        public DateTime? Deathday;
    }
    static class Program
    {
        static void Main()
        {
            string timelineFile = Path.Combine(Environment.CurrentDirectory, "timeline.csv");
            string peopleFile = Path.Combine(Environment.CurrentDirectory, "people.csv");


            var peopleData = ReadDataPeople(peopleFile);
            var timelineData = ReadDataTimeline(timelineFile);

            (int years, int months, int days) = DeltaMinAndMaxDate(timelineData);
            
            Console.WriteLine($"Между макс и мин датами прошло: {years} лет, {months} месяцев и {days} дней");
            Console.Write("Люди, родившиеся в високосный год, чей возраст не более 20: "); 
            DateCheck(peopleData);
        }
        static void DateCheck(Person[] peopleData)
        {
            foreach(var leapYear in peopleData)
            {
                DateTime date = leapYear.Birthday;
                DateTime nowDate = DateTime.Now;
                if (DateTime.IsLeapYear(date.Year) && (nowDate.Year - date.Year <= 20) && (date < nowDate))
                {
                    Console.Write(leapYear.Name);
                    Console.Write(", ");
                }
            }
        }
        static Person[] ReadDataPeople(string path)
        {
            string[] data = File.ReadAllLines(path);
            Person[] splitData = new Person[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                Person @person = default;
                var line = data[i];
                string[] parts = line.Split(';');
                @person.Id = int.Parse(parts[0]);
                @person.Name = parts[1];
                @person.Birthday = ParseDate(parts[2]);
                splitData[i] = @person;
            }
            return splitData;
        }
        static TimelineEvent[] ReadDataTimeline(object path)
        {
            string[] data = File.ReadAllLines(path.ToString());
            TimelineEvent[] splitData = new TimelineEvent[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                TimelineEvent @event = default;
                var line = data[i];
                string[] parts = line.Split(';');
                @event.Passage = parts[1];
                @event.Date = ParseDate(parts[0]);
                splitData[i] = @event;
            }
            return splitData;
        }
        static DateTime ParseDate(string value)
        {
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None,
                    out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None,
                        out date))
                    {
                    }
                }
            }
            return date;
        }
        static (int years, int months, int days) DeltaMinAndMaxDate(TimelineEvent[] timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            TimeSpan difference = (maxDate.Subtract(minDate));
            DateTime correctDate = DateTime.MinValue.Add(difference);
            return (correctDate.Year - DateTime.MinValue.Year,
                correctDate.Month - DateTime.MinValue.Month,
                correctDate.Day - DateTime.MinValue.Day);
        }
        static (DateTime, DateTime) GetMinAndMaxDate(TimelineEvent[] timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            foreach (var timeEvent in timeline)
            {
                DateTime date = timeEvent.Date;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }
            return (minDate, maxDate);
        }
        static void WriteJson(Person[] people, TimelineEvent[] timeline)
        {
            string outTimeline = JsonConvert.SerializeObject(timeline);
            string outPeople = JsonConvert.SerializeObject(people);
            File.WriteAllText(Environment.CurrentDirectory, outTimeline, Encoding.UTF8);
            File.WriteAllText(Environment.CurrentDirectory, outPeople, Encoding.UTF8);
        }
    }
}